package GeometricShapes;
public class Square implements IСalculations{
    double AnySide;

    @Override
    public double getPerimeter() {
        return AnySide * 4;
    }
    @Override
    public int getNumberOfCorners()
    {
        return 4;
    }
}
