package GeometricShapes;
interface IСalculations {
    double getPerimeter();
    int getNumberOfCorners();
}
