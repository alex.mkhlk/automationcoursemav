package GeometricShapes;
public class MainForPropertiesOfGeometricShapes {
    public static void main(String[] args) {
        Triangle triangle = new Triangle();
        triangle.sideOne = 4.5;
        triangle.sideTwo = 4.5;
        triangle.sideThree = 7.0;
        System.out.println("Perimeter of triangle: " + triangle.getPerimeter());
        System.out.println("Number Of Corners: " + triangle.getNumberOfCorners());
        System.out.println();

        Square square = new Square();
        square.AnySide = 9.9;
        System.out.println("Perimeter of square: " + square.getPerimeter());
        System.out.println("Number Of Corners: " + square.getNumberOfCorners());
        System.out.println();

        Hexagon hexagon = new Hexagon();
        hexagon.sideOne = 8.8;
        hexagon.sideTwo = 8.9;
        hexagon.sideThree = 7.9;
        hexagon.sideFour = 7.7;
        hexagon.sideFive = 9.2;
        hexagon.sideSix = 9.0;
        System.out.println("Perimeter of hexagon: " + hexagon.getPerimeter());
        System.out.println("Number Of Corners: " + hexagon.getNumberOfCorners());
    }
}
