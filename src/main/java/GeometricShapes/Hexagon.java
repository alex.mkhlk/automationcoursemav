package GeometricShapes;
public class Hexagon implements IСalculations{
    double sideOne;
    double sideTwo;
    double sideThree;
    double sideFour;
    double sideFive;
    double sideSix;

    public double getPerimeter()
    {
        return sideOne + sideTwo + sideThree + sideFour + sideFive + sideSix;
    }

    public int getNumberOfCorners()
    {
        return 6;
    }
}
