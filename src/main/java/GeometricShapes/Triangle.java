package GeometricShapes;

public class Triangle implements IСalculations {
    double sideOne;
    double sideTwo;
    double sideThree;

   public double getPerimeter() {
        return sideOne + sideTwo + sideThree;
    }

    public int getNumberOfCorners()
    {
        return 3;
    }
}
